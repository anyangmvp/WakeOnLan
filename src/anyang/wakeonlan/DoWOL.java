/**
 * @fileName wakeonlan.DoWOL.java
 * @author Anyang
 * @date 2015-7-2
 */
package anyang.wakeonlan;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 从文件读取MAC列表 (换行分割)，进行网络唤醒远程计算机
 * 
 * @author Anyang
 * @date 2015-7-2
 */
public class DoWOL {
	/**
	 * 从MacListTxt 获取MAC 列表
	 * 
	 * @author Anyang
	 * @date 2015-7-2
	 * @param MACListTxt
	 * @return
	 */
	public static List<String> getMacList(String MACListTxt) {
		List<String> MACList = new ArrayList<String>();
		BufferedReader br = null;
		try {
			String macTemp = null;
			br = new BufferedReader(new FileReader(MACListTxt));
			while ((macTemp = br.readLine()) != null && !macTemp.trim().isEmpty()) {
				MACList.add(macTemp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
		return MACList;
	}

	/**
	 * 从ConfigXMLPath的XML配置文件 获取网络唤醒所需要的配置信息列表
	 * 
	 * @author Anyang
	 * @date 2015-7-3
	 * @param ConfigXMLPath
	 * @return
	 */
	public static List<Map<String, Object>> getConfig(String ConfigXMLPath) {
		List<Map<String, Object>> configList = new ArrayList<Map<String, Object>>();
		SAXReader reader = new SAXReader();
		try {
			reader.setEncoding("UTF-8");
			Document doc = reader.read(new File(ConfigXMLPath));
			Element rootElm = doc.getRootElement();
			List<Element> configElms = rootElm.elements("Config");
			if (configElms != null && configElms.size() > 0) {
				for (Element ConfigElm : configElms) {
					Map<String, Object> configMap = new HashMap<String, Object>();
					List<String> macAddressList = new ArrayList<String>();
					String broadcastAddress = ConfigElm.attributeValue("BroadcastAddress");
					String broadcastPort = ConfigElm.attributeValue("BroadcastPort");
					List<Element> macListElm = ConfigElm.elements("MACAddress");
					if (macListElm != null && macListElm.size() > 0) {
						for (Element macElm : macListElm) {
							macAddressList.add(macElm.getTextTrim());
						}
					} else {
						continue;
					}
					configMap.put("BroadcastAddress", broadcastAddress);
					configMap.put("BroadcastPort", broadcastPort);
					configMap.put("MACAddressList", macAddressList);
					configList.add(configMap);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return configList;
	}

	public static void main(String[] args) {
//		args = new String[]{"G:\\我的文档\\桌面\\WOLConfig.xml"};
		String help = "网络远程唤醒功能执行失败 ，需要传入配置XML文件所在的路径作为执行参数！\n\n" +
				"XML具体格式如下：\n" +
				"<?xml version=\"1.1\" encoding=\"utf-8\"?>                       \n"+
				"<ConfigList>                                                     \n"+
				"  <Config BroadcastAddress=\"10.10.255.255\" BroadcastPort=\"9\">\n"+
				"    <MACAddress>00:0B:AB:77:C6:94</MACAddress>                   \n"+
				"    ...                                                          \n"+
				"  </Config>                                                      \n"+
				"  ...                                                            \n"+
				"</ConfigList>                                                    \n";

		if (args.length != 1) {
			System.out.println(help);
			return;
		}
		String ConfigXMLPath = args[0];
		ExecutorService pool = Executors.newFixedThreadPool(5);
		List<Map<String, Object>> configList = getConfig(ConfigXMLPath);
		for (Map<String, Object> configMap : configList) {
			final String broadcastAddress = (String) configMap.get("BroadcastAddress");
			final String broadcastPort = (String) configMap.get("BroadcastPort");
			List<String> macAddressList = (List<String>) configMap.get("MACAddressList");
			if (!broadcastAddress.matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")) {
				System.out.println("广播地址(BroadcastAddress：" + broadcastAddress + ")格式不正确，导致该列表下的所有MAC地址执行网络远程唤醒失败！");
				continue;
			}
			if (!broadcastPort.matches("^\\d{1,5}$")) {
				System.out.println("广播端口(BroadcastPort：" + broadcastPort + ")格式不正确，导致该列表下的所有MAC地址执行网络远程唤醒失败！");
				continue;
			}
			for (final String mac : macAddressList) {
				pool.execute(new Thread(new Runnable() {
					public void run() {
						try {
							WakeOnLan wol = new WakeOnLan(mac, broadcastAddress, Integer.parseInt(broadcastPort));
							boolean isSuccess = wol.sendMagicPackage();
							System.out.println("【" + broadcastAddress + "】: " + mac + " 发送开机指令" + (isSuccess ? "成功。" : "失败！") + "");
						} catch (Exception e) {
							System.out.println("【" + broadcastAddress + "】: " + mac + " 发送开机指令出错！");
						}
					}
				}));
			}
		}
		pool.shutdown();
	}

}
